rely_on_nobility = {
	global_tax_modifier = -0.25
}

strong_king = {
	global_tax_modifier = 0.25
	recover_army_morale_speed = 0.10
}