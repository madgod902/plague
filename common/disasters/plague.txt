the_plague = {
    potential = {
	    OR = {
		    custom_trigger_tooltip = {
                tooltip = is_plague_origin
            	has_country_flag = plague_origin
			}
			custom_trigger_tooltip = {
	            tooltip = neighbor_is_infected
				OR = {
            		any_neighbor_country = { has_country_flag = had_the_plague } 
    	    		any_neighbor_country = { has_country_flag = has_the_cure }
				}
			}
		}
        NOT = { has_country_flag = had_the_plague }
		NOT = { has_country_flag = has_the_cure }
	}
	can_start = {
	    NOT = { plague_ideas = 7 }
		is_year = 1500
		NOT = { is_year = 1700 }
	}
	can_stop = {
	    plague_ideas = 7
		custom_trigger_tooltip = {
		    tooltip = has_cure_tooltip
			has_country_flag = has_the_cure
		}
	}
	progress = {
	    modifier = {
		    factor = 0.25
			is_nomad = yes
		}
		modifier = {
		    factor = 3
			NOT = { has_country_modifier = closed_country_policy }
		}
		modifier = {
		    factor = 1
			NOT = { plague_ideas = 1 }
		}
	}
	can_end = {
	    OR = {
		    plague_ideas = 7
			has_country_flag = has_the_cure
		}
	}
	modifier = {
	    global_unrest = 3
		land_attrition = 1
		manpower_recovery_speed = -0.25
		sailors_recovery_speed = -0.25
		development_cost = 0.2
		hostile_attrition = 1
		global_tax_modifier = -0.25
		stability_cost_modifier = 0.5
		land_morale = -0.5
		naval_morale = -0.5
		siege_ability = -0.25
		global_trade_goods_size_modifier = -0.15
		global_prov_trade_power_modifier = -0.15
		state_maintenance_modifier = 0.15
	}
	
	on_start = the_plague_events.2
	on_end = the_plague_events.10
	
	on_monthly = {
	    events = {
		}
		random_events = {
		    1000 = 0
			20 = the_plague_events.7
		}
	}
}